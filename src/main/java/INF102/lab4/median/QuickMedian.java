package INF102.lab4.median;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list

        int medianIndex = listCopy.size()/2;

        return findMedian(listCopy, 0, medianIndex);



        /* 
        PriorityQueue<T> low = new PriorityQueue<>(Comparator.reverseOrder());
        PriorityQueue<T> high = new PriorityQueue<>();
        high.add(listCopy.get(0)); //O(1)

        for (int i = 1; i<listCopy.size(); i++) {
            if (listCopy.get(i).compareTo(high.peek())<0) {
			    low.add(listCopy.get(i)); 
		    }
		    else {
			    high.add(listCopy.get(i));
		    }
            if ((low.size() + high.size()) % 2 == 0 && low.size() != high.size()) {
                if (high.size() > low.size()) {
                    low.add(high.remove()); 
                }
                else {
                    high.add(low.remove());
                }
            }
            if ((low.size() + high.size()) % 2 == 1 && low.size() > high.size()) {
                high.add(low.remove());
            }
        }
        return high.peek();
        */
    }

    private <T extends Comparable<T>> T findMedian(List<T> list, int lowerboundIndex, int medianIndex) {
        T pivot = list.get(0);
        ArrayList<T> LESSER = new ArrayList<>();
        ArrayList<T> EQUAL = new ArrayList<>();
        ArrayList<T> GREATER = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(pivot) < 0) {
                LESSER.add(list.get(i));
            }
            else if (list.get(i).compareTo(pivot) > 0) {
                GREATER.add(list.get(i));
            }
            else {
                EQUAL.add(list.get(i));
            }
        }

        if (lowerboundIndex + LESSER.size() > medianIndex) {
            //Median is in LESSER
            return findMedian(LESSER, lowerboundIndex, medianIndex);
        }
        else if (lowerboundIndex + LESSER.size() + EQUAL.size() > medianIndex) {
            //Median is in EQUAL
            return EQUAL.get(0);
        }
        else {
            //Median is in GREATER
            return findMedian(GREATER, lowerboundIndex + LESSER.size() + EQUAL.size(), medianIndex);
        }
    }

}
