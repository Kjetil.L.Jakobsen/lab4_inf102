package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {

        /* 
        for (int i = 0; i < list.size()-1; i++) {
            list.set(i+1, list.get(i));
        }
        */

        //Faktisk bubblesort algoritme:

        
        boolean swapped = true;
        while (swapped) {
            swapped = false;
            for (int i = 0; i < list.size()-1; i++) {
                if (list.get(i).compareTo(list.get(i+1)) > 0) {
                    T temp = list.get(i);
                    list.set(i, list.get(i+1));
                    list.set(i+1, temp);
                    swapped = true;
                }
            }
        }

    }
    
}
